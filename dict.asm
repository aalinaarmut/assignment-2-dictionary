%include "lib.inc"
%define BYTE 8

global find_word

section .text

find_word:
    push r12
    push r13
    mov r12, rsi
    mov r13, rdi

.loop:
     xor rax, rax
     test r12, r12
     je .exit 
  
    add r12, BYTE
    call string_equals
    test rax, rax
    jne .found
    mov r12, [r12]
    jmp .loop

.found:
    mov rax, r12
    pop r13
    pop r12
    ret

.exit:
    xor rax, rax
    pop r13
    pop r12
    ret
