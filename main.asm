%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFFER_SIZE 255

section .bss
    buffer: resb (BUFFER_SIZE + 1)          
section .rodata
    buffer_msg: db "Message is too big for buff",0      
    no_key_msg: db "Key wasn't valid",0

global _start
extern find_word
section .text                                                       
    _start:
        mov rdi, buffer      
        mov rsi, BUFFER_SIZE
        call read_word                  
        test rax,rax   
        jz .buffer_error
                                                  
        mov rdi, buffer                     
        mov rsi, next_elem
        call find_word  
             
        test rax,rax  
        jz .key_error  
        
        lea rdi, [rax+8]                                       
        push rdi
        call string_length                                    
        pop rdi
        add rdi, rax             
        inc rdi         
        call print_string  
        jmp exit
        

        .buffer_error:
            mov rsi, buffer_msg                 
            mov rdx, 32
            jmp .print_err
        .key_error:
            mov rsi, no_key_msg
            mov rdx, 24
        .print_err:
            mov rax, 1                                      
            mov rdi, 2            
            syscall 
         .exit:
            call exit
