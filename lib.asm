section .text

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global print_error



%define EXIT 60
%define IO_STDOUT 1
%define SYS_WRITE 1
%define POINTER_1 1
%define POINTER_0 0

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall
   

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    mov cl, [rdi+rax]
    test cl, cl
    jz .exit
    inc rax
    jmp .loop
    .exit:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi
  call string_length
  pop rdi
  mov rdx, rax
  mov rax, SYS_WRITE
  mov rsi, rdi
  mov rdi, IO_STDOUT
  syscall
  ret
   
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
       print_char:
    push rdi
    mov rsi, rsp
    mov rdi, IO_STDOUT
    mov rax, IO_STDOUT
    mov rdx, IO_STDOUT
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
   xor rax, rax
   test rdi, rdi
   jge print_uint
   push rdi
   mov dil, '-'
   call print_char
   pop rdi
   neg rdi
   

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
  push rbp
  mov rbp, rsp
  mov rax, rdi
  push 0
  mov rdi, rsp
  sub rsp, 20
  mov r10, 10
  .loop:
  xor rdx, rdx
  div r10
  add dl, 0x30
  dec rdi
  mov [rdi], dl
  test rax, rax
  jnz .loop
  push rbp
  call print_string
  mov rsp, rbp
  pop rbp
  ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
    mov r9b, byte[rdi+rcx]
    cmp r9b, byte [rsi+rcx]
    jne .no_equal
    .check:
    test r9b, r9b
    je .equal
    inc rcx
    jmp .loop
    .no_equal:
    xor rax,rax
    ret
    .equal:
    mov rax,  POINTER_1
    ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
  push 0
  mov rax, 0
  xor rdi, rdi
  mov rsi, rsp
  mov rdx, 1
  syscall
  pop rax
  ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

    push rdi
    push rdi
    push rsi
    

    .skip_loop:
    call read_char
    cmp rax, 0x20
    je .skip_loop
    cmp rax , 0x9
    je .skip_loop
    cmp rax , 0xA
    je .skip_loop
    jmp .after_skip_loop

    .read_loop:
    
    call read_char


    .after_skip_loop:
    test rax, rax
    jz .end
    cmp rax, ' '
    je .end
    cmp rax, '\t'
    je .end
    cmp rax, '\n'
    je .end
    cmp qword [rsp], 0
    je .error
    mov rsi, [rsp+8]
    mov byte [rsi],al
    dec qword [rsp]
    inc qword [rsp+8]
    jmp .read_loop
    
 .end:
    pop rcx
    pop rsi
    mov rdi, 0
    mov [rsi], rdi
    pop rdi
    mov rax, rdi
    sub rsi, rdi
    mov rdx, rsi
    ret
    
    .error:
    add rsp, 24
    xor rax, rax
    xor rdx, rdx
    ret 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
 xor rax,rax
 xor rcx, rcx
 xor rdx, rdx
 .loop:
 mov dl, byte[rdi+rcx]
 cmp dl,'0'
 jb .errors
 cmp dl, '9'
 ja .errors
 imul rax,10
 sub rdx, '0'
 add rax, rdx
 inc rcx
jmp .loop
ret
.errors:
mov rdx, rcx
ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
      xor rax, rax
      xor rdx, rdx
    mov al, byte[rdi]
    cmp al, '-'
    jz .negative
    cmp al, '+'    
    jz .positive       
    jmp parse_uint
  .positive:
    inc rdi            
    call parse_uint     
    inc rdx          
    ret
  .negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
  xor rcx, rcx  
  xor rax, rax 
  .loop:
    cmp rcx, rdx  
    jae .o
    mov r8b, byte [rdi+rcx]  
    mov byte [rsi+rcx], r8b  
    inc rcx 
    inc rax
    test r8b, r8b  
    jz .end
    jmp .loop
  .o:
    xor rcx, rcx  
    mov rax, 0
    jmp .end
  .end:
    ret

print_error:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret
