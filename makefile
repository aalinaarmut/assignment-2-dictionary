ASMFLAGS=-felf64

all: main

main: main.o dict.o lib.o
	ld main.o lib.o dict.o -o main

lib.o: lib.asm
	nasm $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc
	nasm $(ASMFLAGS) -o $@ $<

main.o: main.asm words.inc lib.inc dict.inc colon.inc
	nasm $(ASMFLAGS) -o $@ $<

clean:
	rm -rf ./*.o main

test:
	python3 test.py

.PHONY: clean, test
